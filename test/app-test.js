const path = require('path');
const assert = require('yeoman-assert');
const helpers = require('yeoman-test');

describe('generator-fastify-webapp:app', () => {
  beforeEach(() =>
    helpers.run(path.join(__dirname, '../generators/app')).withPrompts({
      applicationName: 'fake-webapp',
    })
  );

  it('should create and CD into a folder named like the generator', () => {
    assert.equal(path.basename(process.cwd()), 'fake-webapp');
  });

  it('creates files', () => {
    const expected = [
      'app/controllers/status.ts',
      'app/services/status.ts',
      'config/middlewares/uniq_id.ts',
      'config/application.ts',
      'config/routes.ts',
      'lib/common_controller.ts',
      'test/controllers/status.spec.ts',
      '.gitignore',
      '.prettierrc',
      'ioc.config',
      'jest.config.js',
      'main.ts',
      'package.json',
      'README.md',
      'tsconfig.build.json',
      'tsconfig.json',
      'tslint.json',
    ];

    assert.file(expected);
  });

  it('should fill files with project data', () => {
    assert.fileContent([['README.md', /# fake-webapp/], ['package.json', /"name": "fake-webapp"/]]);
  });
});
