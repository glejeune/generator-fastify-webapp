# <%- applicationName %>

## Availables npm commandes :

* `start` : start the server
* `build` : compile the project
* `clean` : clean the project (remove the dist directory)
* `test` : run tests
* `test:cov` : run tests with code coverage
