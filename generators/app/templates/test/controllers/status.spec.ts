import * as fastify from 'fastify';
import { Server, IncomingMessage, ServerResponse } from 'http';
import { application } from '../../config/application';
import { IStatusService } from '../../app/services/status';

describe('StatusController', () => {
  let server: fastify.FastifyInstance<Server, IncomingMessage, ServerResponse>;

  beforeEach(async () => {
    server = application(true);
  });

  afterEach(() => {
    server.close();
  });

  describe('/status', () => {
    it('should get server status', done => {
      server.inject(
        {
          method: 'GET',
          url: '/status',
        },
        (err, response) => {
          expect(response.payload).toBe('ok');
          done();
        }
      );
    });
  });
});
