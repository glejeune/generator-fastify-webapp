import { Provides, Singleton, AutoWired } from 'typescript-ioc';

export abstract class IStatusService {
  abstract async status();
}

@Singleton
@AutoWired
@Provides(IStatusService)
export class StatusService implements IStatusService {
  async status() {
    return 'ok';
  }
}
