import { Inject, Singleton, AutoWired, Provides } from 'typescript-ioc';
import { FastifyRequest, FastifyReply, FastifyInstance } from 'fastify';
import { IncomingMessage, ServerResponse } from 'http';
import { IStatusService } from '../services/status';
import { CommonController } from '../../lib/common_controller';

export abstract class IStatusController {
  abstract async statusHandler(request: FastifyRequest<IncomingMessage>, reply: FastifyReply<ServerResponse>);
}

@Singleton
@AutoWired
@Provides(IStatusController)
export class StatusController extends CommonController {
  @Inject
  private readonly statusService: IStatusService;

  public readonly statusOptions: {} = {
    schema: {
      response: {
        200: {
          type: 'string',
        },
      },
    },
  };

  async statusHandler(request: FastifyRequest<IncomingMessage>, reply: FastifyReply<ServerResponse>) {
    return this.statusService.status();
  }
}
