import {AddressInfo} from 'net';
import {application} from './config/application';

const start = async () => {
  const app = application();
  try {
    await app.listen(3000);
  } catch (err) {
    app.log.error(err);
    process.exit(1);
  }
};

start();
