import { FastifyInstance } from 'fastify';
import { Container } from 'typescript-ioc';
import { IStatusController } from '../app/controllers/status';

async function routes(server: FastifyInstance, options: {}) {
  Container.get(IStatusController).get(server, '/status', 'statusOptions', 'statusHandler');
}

export { routes };
