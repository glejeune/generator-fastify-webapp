import { FastifyRequest, FastifyReply } from 'fastify';
import * as uuidv4 from 'uuid/v4';

function uniqID(options = {}) {
  return (request, response, next) => {
    const uid = uuidv4();
    response.setHeader('X-Response-UID', uid);
    next();
  };
}

export { uniqID };
