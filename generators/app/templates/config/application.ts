import * as fastify from 'fastify';
import * as fastifySwagger from 'fastify-swagger';
import { Server, IncomingMessage, ServerResponse } from 'http';
import { ContainerConfig } from 'typescript-ioc/container-config';
import { routes } from './routes';
import { uniqID } from './middlewares/uniq_id';

function application(test = false) {
  ContainerConfig.addSource(['**/*.ts', '!test.ts'], 'baseFolder');
  const server: fastify.FastifyInstance<Server, IncomingMessage, ServerResponse> = fastify({
    logger: {
      level: test ? 'fatal' : 'debug',
    },
  });

  if (!test) {
    server.register(fastifySwagger, {
      routePrefix: '/doc',
      exposeRoute: true,
      swagger: {
        info: {
          title: 'Test swagger',
          description: 'testing the fastify swagger api',
          version: '0.1.0',
        },
        externalDocs: {
          url: 'https://swagger.io',
          description: 'Find more info here',
        },
        host: 'localhost',
        schemes: ['http'],
        consumes: ['application/json'],
        produces: ['application/json'],
        tags: [{ name: '<%- applicationName %>', description: '<%- applicationName %> related end-points' }],
      },
    });
  }

  server.register(routes);
  server.use(uniqID());

  server.ready(err => {
    if (err) {
      throw err;
    }
    if (!test) {
      // nothing
    }
  });

  return server;
}

export { application };
