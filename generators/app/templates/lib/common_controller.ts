import { Provides } from 'typescript-ioc';
import { FastifyInstance } from 'fastify';

export abstract class ICommonController {
  abstract post(server: FastifyInstance, path: string, options: string, handler: string);
  abstract get(server: FastifyInstance, path: string, options: string, handler: string);
  abstract head(server: FastifyInstance, path: string, options: string, handler: string);
  abstract put(server: FastifyInstance, path: string, options: string, handler: string);
  abstract delete(server: FastifyInstance, path: string, options: string, handler: string);
  abstract options(server: FastifyInstance, path: string, options: string, handler: string);
  abstract patch(server: FastifyInstance, path: string, options: string, handler: string);
}

@Provides(ICommonController)
export class CommonController {
  post(server: FastifyInstance, path: string, options: string, handler: string) {
    if (handler) {
      server.post(path, this[options], this[handler].bind(this));
    } else {
      server.post(path, this[options].bind(this));
    }
  }

  get(server: FastifyInstance, path: string, options: string, handler: string) {
    if (handler) {
      server.get(path, this[options], this[handler].bind(this));
    } else {
      server.get(path, this[options].bind(this));
    }
  }

  head(server: FastifyInstance, path: string, options: string, handler: string) {
    if (handler) {
      server.head(path, this[options], this[handler].bind(this));
    } else {
      server.head(path, this[options].bind(this));
    }
  }

  put(server: FastifyInstance, path: string, options: string, handler: string) {
    if (handler) {
      server.put(path, this[options], this[handler].bind(this));
    } else {
      server.put(path, this[options].bind(this));
    }
  }

  delete(server: FastifyInstance, path: string, options: string, handler: string) {
    if (handler) {
      server.delete(path, this[options], this[handler].bind(this));
    } else {
      server.delete(path, this[options].bind(this));
    }
  }

  options(server: FastifyInstance, path: string, options: string, handler: string) {
    if (handler) {
      server.options(path, this[options], this[handler].bind(this));
    } else {
      server.options(path, this[options].bind(this));
    }
  }

  patch(server: FastifyInstance, path: string, options: string, handler: string) {
    if (handler) {
      server.patch(path, this[options], this[handler].bind(this));
    } else {
      server.patch(path, this[options].bind(this));
    }
  }
}
