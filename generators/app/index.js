const path = require('path');
const Generator = require('yeoman-generator');
const mkdirp = require('mkdirp');
const chalk = require('chalk');
const yosay = require('yosay');

module.exports = class extends Generator {
  initializing() {
    this.props = {};
  }

  prompting() {
    this.log(yosay(`Welcome to the funkadelic ${chalk.red('generator-fastify-webapp')} generator!`));

    const prompts = [
      {
        type: 'input',
        name: 'applicationName',
        message: 'Name of your application?',
        default: this.appname,
      },
    ];

    return this.prompt(prompts).then(props => {
      this.props = props;
      return props;
    });
  }

  default() {
    if (path.basename(this.destinationPath()) !== this.props.applicationName) {
      this.log(
        `Your generator must be inside a folder named ${
          this.props.applicationName
        }\nI'll automatically create this folder.`
      );
      mkdirp(this.props.applicationName);
      this.destinationRoot(this.destinationPath(this.props.applicationName));
    }
  }

  writing() {
    const templates = {
      'config/application.ts': 'config/application.ts',
      'README.md': 'README.md',
      'package.json': 'package.json',
    };
    Object.keys(templates).forEach(file => {
      this.fs.copyTpl(this.templatePath(file), this.destinationPath(templates[file]), this.props);
    });

    const files = {
      'app/controllers/status.ts': 'app/controllers/status.ts',
      'app/services/status.ts': 'app/services/status.ts',
      'config/middlewares/uniq_id.ts': 'config/middlewares/uniq_id.ts',
      'config/routes.ts': 'config/routes.ts',
      'lib/common_controller.ts': 'lib/common_controller.ts',
      'test/controllers/status.spec.ts': 'test/controllers/status.spec.ts',
      _gitignore: '.gitignore',
      _prettierrc: '.prettierrc',
      'ioc.config': 'ioc.config',
      'jest.config.js': 'jest.config.js',
      'main.ts': 'main.ts',
      'tsconfig.build.json': 'tsconfig.build.json',
      'tsconfig.json': 'tsconfig.json',
      'tslint.json': 'tslint.json',
    };
    Object.keys(files).forEach(file => {
      this.fs.copy(this.templatePath(file), this.destinationPath(files[file]));
    });
  }

  install() {
    this.installDependencies({
      bower: false,
      npm: true,
    });
  }
};
