# generator-fastify-webapp

> Bootstrap a Fastify webapp

## Installation

First, install [Yeoman](http://yeoman.io) and generator-fastify-webapp.

```bash
npm install -g yo
npm install -g https://gitlab.com/glejeune/generator-fastify-webapp
```

Then generate your new project:

```bash
yo fastify-webapp
```

